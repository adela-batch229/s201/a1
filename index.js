// console.log("Hello")

let number = Number(prompt("Enter a number"))
console.log("The number you provided is " + number)
for(let i = number; i >= 0 ; i--){
	if(i % 10 === 0 && i % 5 === 0){
		console.log("This number is divisible by 10. Skipping the number.");
		continue;
	}
	if(i % 5 === 0){
		console.log(i);
		continue;
	}
	if(i <= 50){
		console.log("The current value is 50. Terminating the loop.")
		break;
	}
}	

let str = "supercalifragilisticexpialidocious";
let consonants = "";

for(let i = 0; i < str.length; i++){
	if(
		str[i].toLowerCase() == 'a' ||
		str[i].toLowerCase() == 'i' ||
		str[i].toLowerCase() == 'o' ||
		str[i].toLowerCase() == 'u' ||
		str[i].toLowerCase() == 'e'
		){
		continue;
	}
	else{
		consonants += str[i];
	}

}
console.log(consonants);